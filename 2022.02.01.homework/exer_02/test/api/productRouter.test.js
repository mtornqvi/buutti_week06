// Refer to :
// https://www.npmjs.com/package/supertest
// and
// https://gitlab.com/sokkanen/my_postgres_app

import { jest } from "@jest/globals";
import request from "supertest";
import app from "../../src/index.js";
import { pool } from "../../src/db/db.js";

const initializeDbMock = (expectedResponse) => {
    pool.connect = jest.fn(() => {
        return {
            query: () => expectedResponse,
            release: () => null
        };
    });
};

describe("Testing Product API", () => {
    it("Root responds OK", async () => {
        const response = await request(app).get("/");
        expect(response.status).toBe(200);
    });

    it("Test : /somelostpoint : returns 404", async () => {
        const response = await request(app).get("/somelostpoint");
        expect(response.status).toBe(404);
    });
});

describe("Testing GET /books", () => {

    const mockResponse = { 
        rows: [
            {
                "id": "0",
                "name": "Robinson Crusoe",
                "author": "Daniel Defoe",
                "read": true
            },
            {
                "id": "1",
                "name": "Count of Monte Cristo",
                "author": "Alexandre Dumas",
                "read": false
            }
        ],
        rowCount : 2
    };


    beforeAll(() => {
        initializeDbMock(mockResponse);
    });

    afterAll(() => {
        jest.clearAllMocks();
    });


    it("Returns 200 with all the products", async () => {
        const response = await request(app).get("/books");
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows);
    });

    it("Returns 500 with a single product when returning multiple products", async () => {
        const response = await request(app).get("/books/0");
        expect(response.status).toBe(500);
    });
   
});

describe("Testing GET /books/id", () => {

    it("Returns 200 with a single book", async () => {
        const mockResponse = {
            rowCount: 1,
            rows: [
                {
                    "id": "1",
                    "name": "Count of Monte Cristo",
                    "author": "Alexandre Dumas",
                    "read": false
                }
            ] 
        };
        initializeDbMock(mockResponse);
        const response = await request(app).get("/books/1");
        expect(response.status).toBe(200);
        expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows[0]);
    });


});
    
describe("Testing GET /books/id, non-FOUND book", () => {
    // Expect not found
    it("Returns 404 with a non-found book", async () => {
        const mockResponse = {
            rowCount: 0,
            rows: [] 
        };
        initializeDbMock(mockResponse);
        const response = await request(app).get("/books/4");
        expect(response.status).toBe(404);        
    });   
});

describe("Testing POST /books/id", () => {
   
    it("Posting a new book", async () => {
        const mockResponse = {
            rowCount: 0,
            rows: [] 
        };
        initializeDbMock(mockResponse);
        const response = await request(app).post("/books/")
            .send ({
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko",
                "read": true
            } );
        expect(response.status).toBe(201);    
        expect("Created");    
    });   

 
    it("Posting a new book with incorrect parameters", async () => {
        const mockResponse = { // ignored
            rowCount: 0,
            rows: [] 
        };
        initializeDbMock(mockResponse);
        const response = await request(app).post("/books/")
            .send( {
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko"
            } );
        expect(response.status).toBe(500); 
    });   

});

describe("Testing PUT /books/:id", () => {

    it("Updating a book, happy case", async () => {
        const mockResponse = {
            rowCount: 1,
            rows: [] // ignored
        };
        initializeDbMock(mockResponse);
        const response = await request(app).put("/books/1")
            .send ({
                "id" : 1,
                "name" : "Mustanaamio",
                "author" : "mikko",
                "read": true
            } );
        expect(response.status).toBe(200);    
        expect("Updated");    
    });   

    it("Put : /books/1 with incorrect parameters", async () => {
        const mockResponse = {
            rowCount: undefined, 
            rows: [] // ignored
        };
        initializeDbMock(mockResponse);
        const response = await request(app).put("/books/1")
            .send( {
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko"
            } );
        expect(response.status).toBe(500);   
    });  

    it("Put : /books/15, try to update non-existing book", async () => {
        const mockResponse = {
            rowCount: 0, 
            rows: [] // ignored
        };
        initializeDbMock(mockResponse);
        const response = await request(app).put("/books/15")
            .send( {
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko",
                "read": true
            } );
        expect(response.status).toBe(404);   
    });  

});


describe("Testing DELETE /books/:id", () => {

    it("Deleting an existing book, happy case", async () => {
        const mockResponse = {
            rowCount: 1,
            rows: [] // ignored
        };
        initializeDbMock(mockResponse);
        const response = await request(app).delete("/books/1");
        expect(response.status).toBe(200);      
    });   

    it("Trying to delete non-existing book", async () => {
        const mockResponse = {
            rowCount: undefined,
            rows: [] // ignored
        };
        initializeDbMock(mockResponse);
        const response = await request(app).delete("/books/15");
        expect(response.status).toBe(404);      
    });  

});

