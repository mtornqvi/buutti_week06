# Learned new things

## decentralized elegant functions

/api contains all the express endpoints in appropriate routers. Routers should never
bypass services. If you have some dao imported to a router, you are doing it wrong.
- /dao contain data access. These files contain the SQL queries and are responsible
for performing the database operations
- /service contains files containing the business logic. Routers from /api folder use
services. Services use Daos to perform database operations

