/* Sample SQL to populate empty books table */
INSERT INTO books (id,name,author,read)
VALUES 
    (0, 'Robinson Crusoe', 'Daniel Defoe', true),
    (1, 'Count of Monte Cristo', 'Alexandre Dumas', false);
    
