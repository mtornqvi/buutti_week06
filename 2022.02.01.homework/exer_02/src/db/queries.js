// Refer to : https://www.postgresql.org/docs/13/datatype.html
// version 13

const createBooksTable = `
    CREATE TABLE IF NOT EXISTS "books" (
	    "id" VARCHAR(36) NOT NULL,
	    "name" VARCHAR(100) NOT NULL,
	    "author" VARCHAR(100) NOT NULL,
        "read"  BOOLEAN NOT NULL,
	    PRIMARY KEY ("id")
    );`;

const createCustomerTable = `
    CREATE TABLE IF NOT EXISTS "customer" (
	    "id" VARCHAR(36) NOT NULL UNIQUE,
	    "username" VARCHAR(100) NOT NULL UNIQUE,
	    "passhash" VARCHAR(100) NOT NULL,
	    PRIMARY KEY ("id")
    );`;

const emptyBooksTable = `
    DELETE FROM "books";
    `;

const initBooksTable = `
	INSERT INTO books (id,name,author,read)
	VALUES 
    (0, 'Robinson Crusoe', 'Daniel Defoe', true),
    (1, 'Count of Monte Cristo', 'Alexandre Dumas', false);
`;

export default { 
    createBooksTable,  createCustomerTable, emptyBooksTable, initBooksTable
};