// Refer to : https://www.postgresql.org/docs/13/datatype.html
// version 13

import { v4 as uuidv4 } from "uuid";
import db from "../db/db.js";

const findAll = async () => {
    console.log("Requesting for all books...");
    const result = await db.executeQuery(
        "SELECT * FROM \"books\";"
    );
    console.log(`Found ${result.rowCount} books.`);
    return result;
};

const findBook = async (reqID) => {
    console.log(`Requesting for book with ID ${reqID}`);
    const query = {
        text: "SELECT * FROM \"books\" WHERE \"id\" = ($1)",
        values: [reqID],
    };
    const result = await db.executeQuery(query);
    console.log(`Found ${result.rowCount} book(s).`);
    if (result.rowCount > 1) {
        const err = new Error("Internal error : Found multiple books.");
        err.name = "dbError";
        throw err;
    } else {
        return result;
    }
};

const insertBook = async (book) => {
    console.log("Inserting a new book...");
    const query = {
        text: "INSERT INTO \"books\" (\"id\",\"name\",\"author\",\"read\") VALUES ($1,$2,$3,$4)",
        values: [...Object.values(book)],
    };
    console.log([...Object.values(book)]);
    const result = await db.executeQuery(query);
    console.log("New book inserted successfully.");
    return result;
};

const updateBook = async (book, reqID) => {
    console.log(`Updating a book with ID ${reqID}`);
    const query = {
        text: "UPDATE \"books\" SET \"id\" = $1,\"name\" = $2,\"author\" = $3,\"read\" = $4 WHERE \"id\" = $5",
        values: [...Object.values(book),reqID],
    };
    console.log("New information provided:");
    console.log([...Object.values(book)]);
    const result = await db.executeQuery(query);
    /**
    console.log("Result:")
    console.table(result);
    console.table(result.rowCount);
     */
    return result;
};

const deleteBook = async (reqID) => {
    console.log(`Deleting a book with ID ${reqID}`);
    const query = {
        text: "DELETE FROM \"books\" WHERE \"id\" = ($1)",
        values: [reqID],
    };
    const result = await db.executeQuery(query);
    console.log(`Deleted ${result.rowCount} book(s).`);
    return result.rowCount;
};

export default {
    findAll, findBook,insertBook,updateBook,deleteBook
};