import express from "express";
import dotenv from "dotenv";
import db from "./db/db.js";
process.env.NODE_ENV != "prod" && dotenv.config();
import { v4 as uuidv4 } from "uuid";

const {APP_PORT} = process.env;

const app = express();
app.use(express.json());

// Exer 1 a: creating tables
// must first await that tables are created
await db.createTables();

// Exer 1 b : insert data into a table
// \"  => literal "
const insertIntoTable = async () => {
    const params = [uuidv4(),"testproduct2",Math.round(500*Math.random())];
    const result = await db.executeQuery(
        "INSERT INTO \"products\" (\"id\",\"name\",\"price\") VALUES ($1, $2, $3);",
        params
    );
    if (result) {
        console.log("Product inserted.");
    } else { 
        console.log("Production insertion failed.");
    }

};

// must await => otherwise 1 c will product error result
await insertIntoTable();

// Exer 1 c : Selecting columns from a table
const findAll = async () => {
    const result = await db.executeQuery(
        "SELECT (id,name) FROM \"products\";"
    );
    console.log(`Found ${result.rowCount} products.`);
    console.log(result.rows);

};

await findAll();

// Exer 1 d : Selecting columns from a table with where 
const findSomeProducts = async () => {
    const result = await db.executeQuery(
        "SELECT * FROM \"products\" WHERE \"price\" < 210;");
    console.log(`Found ${result.rowCount} products.`);
    console.log(result.rows);

};
await findSomeProducts();

// Exer 1 e : Deleting some columns from a table with where 
console.log("Deleting some products.");
const deleteSomeProducts = async () => {
    const result = await db.executeQuery(
        "DELETE FROM \"products\" WHERE \"price\" < 210;");
    console.log(`Deleted ${result.rowCount} products.`);
};
await deleteSomeProducts();

// Exer 1 f : Updating a row(s) in a table
console.log("Updating some products.");
const updateSomeProducts = async () => {
    const result = await db.executeQuery(
        "UPDATE \"products\" SET \"price\" = 0.5 * \"price\";");
    console.log(`Updated ${result.rowCount} products.`);
};
await updateSomeProducts();

app.listen(APP_PORT, () => {
    console.log(`Example app listening on port ${APP_PORT}`);
});