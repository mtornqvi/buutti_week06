import pg from "pg";
import dotenv from "dotenv";
import queries from "./queries.js";

process.env.NODE_ENV != "prod" && dotenv.config();

const {DB_USER,DB_HOST,DB_DATABASE,DB_PASSWORD,DB_PORT} = process.env;

const pool = new pg.Pool({
    user : DB_USER,
    host : DB_HOST,
    database: DB_DATABASE,
    password: DB_PASSWORD,
    port : DB_PORT,
});

const createTables = async () => {
    const result = await Promise.all([
        await executeQuery(queries.createProductTable),
        await executeQuery(queries.createCustomerTable)
    ]);
    console.log(result ? 
        "Tables created (or previously existed) successfully." : 
        "Error in initializing database"
    );
};

// Parameters are expected to be passed as an array of primitives
const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    try {
        const result = await client.query(query, parameters);
        return result;
    } catch (error) {
        console.error(error.stack);
        throw error;
    } finally {
        client.release();
    }
};

export default {
    executeQuery,
    createTables
};
