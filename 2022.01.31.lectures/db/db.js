import pg from "pg";
import dotenv from "dotenv";
process.env.NODE_ENV != "prod" && dotenv.config();

const {DB_USER,DB_HOST,DB_DATABASE,DB_PASSWORD,DB_PORT} = process.env;

const pool = new pg.Pool({
    user : DB_USER,
    host : DB_HOST,
    database: DB_DATABASE,
    password: DB_PASSWORD,
    port : DB_PORT,
});

const createTestTable = 
`CREATE TABLE IF NOT EXISTS
testitaulu(id int, nimi varchar(255));`;
// "SELECT NOW()"

export const intializeDatabase = () => {

    pool.query(createTestTable, (err,res) => {
        console.log(err,res);
        pool.end();
    });
};

export default intializeDatabase;
