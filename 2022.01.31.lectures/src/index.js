import express from "express";
import { intializeDatabase } from "../db/db.js";
import dotenv from "dotenv";
process.env.NODE_ENV != "prod" && dotenv.config();

const {APP_PORT} = process.env;

const app = express();

intializeDatabase();

app.listen(APP_PORT, () => {
    console.log(`Example app listening on port ${APP_PORT}`);
});